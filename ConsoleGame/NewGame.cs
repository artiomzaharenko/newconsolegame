﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ConsoleGame
{
    class NewGame
    {
        static bool isValidInput;
        Player newPlayer = new Player();
        bool isGameEnded = false;
        bool isPlayerFinished = false;
        bool isPlayerDead = false;
        bool isWantToExit = false;
        static ConsoleKeyInfo input = new ConsoleKeyInfo();



        public void Run()
        {
            Messages.ShowIntro();
            Messages.ShowReadyToStartText();
            ConfirmGameStart(out input);
        }

        private static void ConfirmGameStart(out ConsoleKeyInfo userInput)
        {
            
            NewGame game = new NewGame();
            
            do
            {
                userInput = Console.ReadKey();
                ValidateRunGameInput(userInput);
                if (!isValidInput)
                {
                    Messages.ShowInvalidInputText();
                }
            } while (!isValidInput);

            switch (userInput.Key)
            {
                case ConsoleKey.Y:
                    while (!game.isWantToExit)
                    {
                        game.GameMechanic();
                    }
                    break;
                case ConsoleKey.N:
                    game.isWantToExit = true;
                    Messages.ShowExitText();
                    System.Environment.Exit(0);
                    break;
            }
        }

        private static bool ValidateRunGameInput(ConsoleKeyInfo userInput)
        {
            if (userInput.Key == ConsoleKey.Y
                  || userInput.Key == ConsoleKey.N)
            {
                isValidInput = true;
            }
            else
            {
                isValidInput = false;
            }
            return isValidInput;
        }

        private static void GetMovementInput(out ConsoleKeyInfo userInput)
        {
            do
            { 
                userInput = Console.ReadKey();
                ValidateMovementInput(userInput);
                if (!isValidInput)
                {
                    Messages.ShowInvalidInputText();
                }
            } while (!isValidInput);
        }
        private static bool ValidateMovementInput(ConsoleKeyInfo userInput)
        {
            if (userInput.Key == ConsoleKey.LeftArrow
                  || userInput.Key == ConsoleKey.RightArrow
                  || userInput.Key == ConsoleKey.UpArrow
                  || userInput.Key == ConsoleKey.DownArrow
                  || userInput.Key == ConsoleKey.W
                  || userInput.Key == ConsoleKey.A
                  || userInput.Key == ConsoleKey.S
                  || userInput.Key == ConsoleKey.D)
            {
                isValidInput = true;
            }
            else
            {
                isValidInput = false;
            }
            return isValidInput;
        }

        private static void GetHit(Player plr, Field fld, out bool isDead)
        {
            isDead = false;
            int damage;
            if (fld.trapMap[plr.PositionY, plr.PositionX] != 0)
            {

                plr.Health -= fld.trapMap[plr.PositionY, plr.PositionX];
                if (plr.Health < 0)
                {
                    plr.Health = 0;
                }
                damage = fld.trapMap[plr.PositionY, plr.PositionX];
                Messages.ShowTrapAlertText(damage, plr);
            }
            if (plr.Health == 0)
            {
                isDead = true;
            }

        }

        private static void GetFinish(Player plr, out bool isFinished)
        {
            isFinished = false;
            if (plr.PositionY == 9 && plr.PositionX == 9)
            {
                isFinished = true;
            }
        }

        private void GameMechanic ()
        {
            Field gameFld = new Field();
            gameFld.CreateTrapMap(gameFld);
            gameFld.EditField(newPlayer, gameFld);
            gameFld.RenderField(gameFld, newPlayer);
                while (!isGameEnded)
                {
                    CheckGameEndCondition(isPlayerDead, isPlayerFinished);
                    GetMovementInput(out input);
                    newPlayer.Moving(input);
                    gameFld.EditField(newPlayer, gameFld);
                    gameFld.RenderField(gameFld, newPlayer);
                    GetHit(newPlayer, gameFld, out isPlayerDead);
                    GetFinish(newPlayer, out isPlayerFinished);
                }
                


        }

        private void CheckGameEndCondition (bool isDead, bool isFinished)
        {
            if (isDead)
            {
                isGameEnded = true;
                Messages.ShowFailText();
                Messages.ShowReadyToStartText();
                ConfirmGameStart(out input);
            }
            if (isFinished)
            {
                isGameEnded = true;
                Messages.ShowSuccesText();
                Messages.ShowReadyToStartText();
                ConfirmGameStart(out input);
            }

        }
    }
}
