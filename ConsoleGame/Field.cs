using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleGame
{
    class Field
    {
        static int fieldSize = 10;
        public char [,] gameField = new char[fieldSize, fieldSize];
        public int [,] trapMap = new int[fieldSize,fieldSize];
        
        
        
        public void CreateTrapMap(Field fld)
        {
            Random rnd = new Random();
            for (int i = 0; i < Trap.amount; i++)
		    {
			    fld.trapMap[1,i] = rnd.Next(Trap.minDamage, Trap.maxDamage);
		    }
		    ShuffleArray(fld.trapMap);
        }
        
        public void EditField (Player plr, Field fld)
        {
            
            for (int i = 0; i < fieldSize; i++)
            {
                for (int j = 0; j < fieldSize; j++)
                {
                    if (fld.trapMap[i, j] != 0 && i == plr.PositionY && j == plr.PositionX)
                    {
                        fld.gameField[i, j] = 'T';
                    }
                    else if (i == plr.PositionY && j == plr.PositionX && fld.trapMap[i, j] == 0)
                    {
                        fld.gameField[i, j] = 'P';
                    }
                    else if (fld.trapMap[i,j] == 0)
                    {
                        fld.gameField[i, j] = ' ';
                    }
                    fld.gameField[9, 9] = 'E';
                        
                }
            
            }
        }

        public void RenderField (Field fld, Player plr)
        {
            Console.Clear();
            
            Console.WriteLine("+---+---+---+---+---+---+---+---+---+---+");
            
            for (int i = 0; i < fieldSize; i++)
            {
                for (int j = 0; j < fieldSize; j++)
                {
                    
                    Console.Write($": {fld.gameField[i,j]} ");
                }
                Console.WriteLine(":");
                Console.WriteLine("+---+---+---+---+---+---+---+---+---+---+");
            }
            Messages.ShowDecriptionText(plr);
        }

        
        
        private static void ShuffleArray(int[,] a)
        {
            Random rand = new Random();
            for (int i = 0; i < 10; i++)
            {
                for(int j = 0; j < 10; j++)
                {
                    Swap(a, i + rand.Next(10 - i), j + rand.Next(10 - j), i, j);
                }
            }
        }
        
        static void Swap(int[,] arr, int changeR, int changeC, int a, int b)
        {
            do
            { 
            int temp = arr[a, b];
            arr[a, b] = arr[changeR, changeC];
            arr[changeR, changeC] = temp;
            } while (arr[0, 0] != 0 && arr[9, 9] != 0);
        }
    }
}
