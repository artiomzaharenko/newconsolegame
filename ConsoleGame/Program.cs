﻿using System;

namespace ConsoleGame
{
    class Program
    {
        static void Main(string[] args)
        {
            NewGame game = new NewGame();
            game.Run();
        }
    }
}
