﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleGame
{
    static class Messages
    {
        public static void ShowIntro()
        {
            Console.WriteLine("********************Console Game!************************");
            Console.WriteLine("Hello! Your task is very simple. You have to go to the");
            Console.WriteLine("end point from your start position using WASD! Be careful!");
            Console.WriteLine("There are 10 hidden traps! You need to avoid them, because");
            Console.WriteLine("you will die! Enjoy it and I hope you won't die! :)");
            Console.WriteLine();
        }
        
        public static void ShowTrapAlertText(int trapDamage, Player plr)
        {
            Console.WriteLine();
            Console.Beep();
            Console.WriteLine($"Be careful, buddy! You trapped into a trap and lost {trapDamage} health point! Only {plr.Health} points left!");
            
        }


        public static void ShowSuccesText()
        {
            Console.WriteLine();
            Console.Beep();
            Console.WriteLine("Congratulation! You win! Maybe you want to repeat?");
            Console.WriteLine();
        }

        public static void ShowFailText()
        {
            Console.WriteLine();
            Console.Beep();
            Console.WriteLine("Sorry, but you died! Want to retry?");
            Console.WriteLine();
        }

        public static void ShowInvalidInputText()
        {
            Console.WriteLine();
            Console.WriteLine("Error! You've pressed wrong key! Try again!");
            Console.WriteLine();
        }

        public static void ShowReadyToStartText()
        {
            Console.Write("Press Y for play new game, press N to exit: ");
        }

        public static void ShowExitText()
        {
            Console.Clear();
            Console.WriteLine("Good bye!");
        }
        
        public static void ShowDecriptionText(Player plr)
        {
            Console.WriteLine($"Your health: {plr.Health}");
            Console.WriteLine("Use WASD or directon arrow keys for move");
            Console.WriteLine("P - your current position, E - exit, T - traps, avoid them");
        }
        
    }
}
