﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace ConsoleGame
{
    class Player
    {
        public int Health { get; set; } = 10;
        public enum Directions
        {
            Up,
            Down,
            Left,
            Right
        }
        public int PositionX { get; set; } = 0;
        public int PositionY { get; set; } = 0;


        public void Moving (ConsoleKeyInfo input)
        {
            if (input.Key == ConsoleKey.W || input.Key  == ConsoleKey.UpArrow)
            {
                MakeAMove(Directions.Up);
            }
            else if(input.Key == ConsoleKey.A || input.Key == ConsoleKey.LeftArrow)
                {
                    MakeAMove(Directions.Left);
                }
            else if (input.Key == ConsoleKey.S || input.Key == ConsoleKey.DownArrow)
                {
                    MakeAMove(Directions.Down);
                }
            else if (input.Key == ConsoleKey.D || input.Key == ConsoleKey.RightArrow)
            {
                MakeAMove(Directions.Right);
            }

        }

        private void MakeAMove (Directions input)
        {
            switch (input)
            {
                case Directions.Up:
                    if (PositionY != 0)
                    {
                        PositionY -= 1;
                    }
                    break;
                case Directions.Left:
                    if (PositionX != 0)
                    {
                        PositionX -= 1;
                    }
                    break;
                case Directions.Down:
                    if (PositionY != 9)
                    {
                        PositionY += 1;
                    }
                    break;
                case Directions.Right:
                    if (PositionX != 9)
                    { 
                        PositionX += 1;
                    }
                    break;
            }
        }
    }
}
